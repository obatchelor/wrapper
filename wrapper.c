
extern "C" {
  
  #include <TH/TH.h>
  #include <luaT.h>

  #include <lua.h>
  #include <lauxlib.h>
  
  #include <lualib.h>   

}


#include <fstream>
#include <sstream>

#include <vector>

extern "C" {

  static int traceback(lua_State *L) {   
    lua_getfield(L, LUA_GLOBALSINDEX, "debug");
    lua_getfield(L, -1, "traceback");
    lua_pushvalue(L, 1);
    lua_pushinteger(L, 2);
    lua_call(L, 2, 1);
    
    fprintf(stderr, "%s\n", lua_tostring(L, -1));
    return 1;
  }

}

int pushTrace(lua_State *L) {
  lua_pushcfunction(L, traceback);
  return lua_gettop(L) - 1;   
}


void pushMember(lua_State *L, const char *name) {
  const char *module = "classify";
  
  lua_getfield(L, LUA_GLOBALSINDEX, module);
  lua_getfield(L, -1, name);
}



// return classification with the confidence, -1 for error
static int classify(lua_State *L, float *confidence, int width, int height, int channels, unsigned char* buffer) {
   
  THByteStorage *storage = THByteStorage_newWithData(buffer, height *  width * channels);
  
  int stride1 = width * channels;
  int stride2 = channels;
  int stride3 = 1;
  
  THByteTensor *tensor = THByteTensor_newWithStorage3d(storage, 0, height, stride1, width, stride2, channels, stride3);
  
  int trace = pushTrace(L);
  
  // Push function
  pushMember(L, "classify");
  
  // Push argument
  luaT_pushudata(L, tensor, "torch.ByteTensor");

  // 1 argument, 2 return values
  if(lua_pcall(L, 1, 2, trace)) {
    return -1; 
  }
  
  
  int label = lua_tonumber(L, -2);
  *confidence = lua_tonumber(L, -1);
  
  lua_pop(L, 3);  // pop return values + error handler
 
  storage->data = NULL;
//   
//   THByteTensor_free(tensor);
//   THByteStorage_free(storage);
  
  return label;
}


// Load serialized network parameters from some external source
static int load(lua_State *L, int size, char *buffer) {
  THCharStorage *storage = THCharStorage_newWithData(buffer, size);
  
  int trace = pushTrace(L);
  pushMember(L, "load");
  
    // Push argument
  luaT_pushudata(L, storage, "torch.CharStorage");
  // 1 argument, 0 return values
  if(lua_pcall(L, 1, 0, trace)) {
    fprintf(stderr, "%s\n", lua_tostring(L, -1)); 
    return 1;
  }
  
  lua_pop(L, 1); // pop error handler
  
  storage->data = NULL;
//   
//   THCharStorage_free(storage);
  return 0;
}


  
static lua_State *init() {

  lua_State *L = luaL_newstate();
  luaL_openlibs(L); 

  int trace = pushTrace(L);
  
  const char *package = "classify";
  
  // load our lua classification package
  lua_getglobal(L, "require");
  lua_pushstring(L, package);  
  
  // one argument no return value
  if(lua_pcall(L, 1, 0, trace)) {
    
    fprintf(stderr, "%s\n", lua_tostring(L, -1));
    
    // Failed to load
    lua_close(L); 
    return NULL;  
  }
  
  lua_pop(L, 1); // pop error handler
    
  return L;
}


static void close(lua_State *L) {
  lua_close(L); 
}


static std::vector<char> readAll(char const* filename)
{
    using namespace std;
  
    ifstream ifs(filename, ios::binary|ios::ate);
    size_t pos = ifs.tellg();

    vector<char> result(pos + 1);

    ifs.seekg(0, ios::beg);
    ifs.read(&result[0], pos);

    return result;
}

//
// gcc wrapper.c  -o wrapper -lluajit -ltorch -lluaT -lTH  
//

int main(int argc, char **argv) {
  
  lua_State *L = init();
  if(L) {
  
    std::vector<char> model = readAll("model.t7");   
    int result = load(L, model.size(), &model[0]);
    
    if(result == -1) {
      printf("Failed to load model\n");
    }
    
    int width = 128;
    int height = 128;
    int channels = 3;
    
    std::vector<unsigned char> image(width * height * channels);
      
    float confidence = 0.0f;
    int label = classify(L, &confidence, width, height, channels, &image[0]);
    
    printf("result: %d, confidence: %f\n", label, confidence);
   
    lua_gc(L, LUA_GCCOLLECT, 0);
    
    close(L);
  } else {
   printf("Failed to initialize\n"); 
    
  }
}

